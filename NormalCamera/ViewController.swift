//
//  ViewController.swift
//  NormalCamera
//
//  Created by SHOKI TAKEDA on 3/20/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit
import Social
import Foundation
import CoreImage
import GPUImage

extension String {
    
    /// String -> NSString に変換する
    func to_ns() -> NSString {
        return (self as NSString)
    }
    
    func substringFromIndex(index: Int) -> String {
        return to_ns().substringFromIndex(index)
    }
    
    func substringToIndex(index: Int) -> String {
        return to_ns().substringToIndex(index)
    }
    
    func substringWithRange(range: NSRange) -> String {
        return to_ns().substringWithRange(range)
    }
    
    var lastPathComponent: String {
        return to_ns().lastPathComponent
    }
    
    var pathExtension: String {
        return to_ns().pathExtension
    }
    
    var stringByDeletingLastPathComponent: String {
        return to_ns().stringByDeletingLastPathComponent
    }
    
    var stringByDeletingPathExtension: String {
        return to_ns().stringByDeletingPathExtension
    }
    
    var pathComponents: [String] {
        return to_ns().pathComponents
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
        return to_ns().stringByAppendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        return to_ns().stringByAppendingPathExtension(ext)
    }
    
}

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NADViewDelegate {
    
    let nendID = "025225932ed056d18d61fbc1e1e4a72e5c37ebb9"
    let spotID = "588937"
    var upCameraBool:Bool = true
    var globalImgWidth:CGFloat = 0
    var globalImgHeight:CGFloat = 0
    var globalImg:UIImage?
    var globalRatio:CGFloat = 0
    var postImg:UIImage?
    var originalImg:UIImage?
    
    var globalUnderLeftX:CGFloat = 0
    var globalUnderLeftY:CGFloat = 0
    var globalUnderWidth:CGFloat = 0
    var globalUnderHeight:CGFloat = 0
    var globalUnderRightX:CGFloat = 0
    var globalUnderRightY:CGFloat = 0
    var globalNoseX:CGFloat = 0
    var globalNoseY:CGFloat = 0
    var globalNoseWidth:CGFloat = 0
    var globalNoseHeight:CGFloat = 0
    
    var underLeftBool:Bool = false
    var underRightBool:Bool = false
    var noseBool:Bool = false
    
    var myViewFrame1: CGRect = CGRectMake(0, 0, 0, 0)
    var myViewFrame2: CGRect = CGRectMake(0, 0, 0, 0)
    var myViewFrame3: CGRect = CGRectMake(0, 0, 0, 0)
    
    var shipImageView : UIImageView!
    var shipGPUImageView : GPUImageView!
    var myImageView1:UIImageView!
    var myImageView2:UIImageView!
    var myImageView3:UIImageView!
    var myImageView4:UIImageView!
    var myImageView5:UIImageView!
    var myImageView6:UIImageView!
    var myImageView7:UIImageView!
    var myImageView8:UIImageView!
    var myImageView9:UIImageView!
    
    private var nadView: NADView!
    var adTimer:NSTimer!
    
    @IBOutlet weak var bottomBack: UIImageView!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var topImgHeight: NSLayoutConstraint!
    @IBOutlet weak var topImgWidth: NSLayoutConstraint!
    @IBOutlet weak var instagramBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var cameraImgView: UIImageView!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    
    @IBOutlet weak var img1: UIButton!
    @IBOutlet weak var img2: UIButton!
    @IBOutlet weak var img3: UIButton!
    @IBOutlet weak var img4: UIButton!
    @IBOutlet weak var img5: UIButton!
    @IBOutlet weak var img6: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID(nendID,
                          spotID: spotID)
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        twitterBtn.backgroundColor = UIColor.clearColor()
        twitterBtn.setImage(UIImage(named: "twitter-big.jpg"), forState: .Normal)
        twitterBtn.imageView!.contentMode = .ScaleAspectFit
        
        fbBtn.backgroundColor = UIColor.clearColor()
        fbBtn.setImage(UIImage(named: "facebook-big.jpg"), forState: .Normal)
        fbBtn.imageView!.contentMode = .ScaleAspectFit
        
        instagramBtn.backgroundColor = UIColor.clearColor()
        instagramBtn.setImage(UIImage(named: "instagram"), forState: .Normal)
        instagramBtn.imageView!.contentMode = .ScaleAspectFit
        
        saveBtn.backgroundColor = UIColor.clearColor()
        saveBtn.setImage(UIImage(named: "save-big"), forState: .Normal)
        saveBtn.imageView!.contentMode = .ScaleAspectFit
        
        cameraBtn.backgroundColor = UIColor.clearColor()
        cameraBtn.setImage(UIImage(named: "camera-big"), forState: .Normal)
        cameraBtn.imageView!.contentMode = .ScaleAspectFit
        
        selectBtn.backgroundColor = UIColor.clearColor()
        selectBtn.setImage(UIImage(named: "album"), forState: .Normal)
        selectBtn.imageView!.contentMode = .ScaleAspectFit
        
        shipGPUImageView = GPUImageView()
        
        btn1.hidden = true
        btn2.hidden = true
        btn3.hidden = true
        btn4.hidden = true
        btn5.hidden = true
        btn6.hidden = true
        
        img1.hidden = true
        img2.hidden = true
        img3.hidden = true
        img4.hidden = true
        img5.hidden = true
        img6.hidden = true
        
        bottomBack.hidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if upCameraBool == false && globalImg != nil {
            if shipImageView != nil {
                shipImageView.removeFromSuperview()
            }
            if myImageView1 != nil {
                myImageView1.removeFromSuperview()
            }
            if myImageView2 != nil {
                myImageView2.removeFromSuperview()
            }
            if shipGPUImageView != nil {
                shipGPUImageView.removeFromSuperview()
            }
            
            globalRatio = globalImg!.size.width / globalImg!.size.height
            globalImgHeight = self.view.frame.height - 200
            globalImgHeight = 368
            globalImgWidth = globalImgHeight * globalRatio
            
            let myImage : UIImage = UIImage.ResizeÜIImage(globalImg!, width: self.view.frame.width, height: self.view.frame.height)
            let options : NSDictionary = NSDictionary(object: CIDetectorAccuracyHigh, forKey: CIDetectorAccuracy)
            let detector : CIDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options as! [String : AnyObject])
            let faces : NSArray = detector.featuresInImage(CIImage(image: myImage)!)
            var transform : CGAffineTransform = CGAffineTransformMakeScale(1, -1)
            transform = CGAffineTransformTranslate(transform, 0, -self.view.frame.height)
            var globalLeftEyeX:CGFloat = 0
            var globalLeftEyeY:CGFloat = 0
            var globalRightEyeX:CGFloat = 0
            var globalRightEyeY:CGFloat = 0
            var globalFaceMinX:CGFloat = 0
            var globalFaceMaxX:CGFloat = 0
            var globalFaceMinY:CGFloat = 0
            var globalFaceMaxY:CGFloat = 0
            var globalMouthX:CGFloat = 0
            var globalMouthY:CGFloat = 0
            var faceCounter:Int = 0
            for feature in faces {
                if faceCounter == 0 {
                    globalLeftEyeX = feature.leftEyePosition.x
                    globalLeftEyeY = feature.leftEyePosition.y
                    globalRightEyeX = feature.rightEyePosition.x
                    globalRightEyeY = feature.rightEyePosition.y
                    globalFaceMinX = feature.bounds.minX
                    globalFaceMaxX = feature.bounds.maxX
                    globalFaceMinY = feature.bounds.minY
                    globalFaceMaxY = feature.bounds.maxY
                    globalMouthX = feature.mouthPosition.x
                    globalMouthY = feature.mouthPosition.y
                }
                faceCounter++
            }
            
            print(originalImg?.size.height)
            print(originalImg?.size.width)
            
            let ciImage:CIImage = CIImage(image:originalImg!)!
            let ciFilter:CIFilter = CIFilter(name: "YUCISurfaceBlur")!
            ciFilter.setValue(ciImage, forKey: "inputImage")
            let ciContext:CIContext = CIContext(options: nil)
            let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
            let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
            
            print(image2?.size.height)
            print(image2?.size.width)
            
            let widthCenter:Double = Double(globalMouthX / self.view.frame.width)
            let heightCenter:Double = Double((self.view.frame.height - globalMouthY) / self.view.frame.height)
            let sourcePicture:GPUImagePicture = GPUImagePicture(image:image2)
            let sobelFilter:GPUImageBulgeDistortionFilter = GPUImageBulgeDistortionFilter()
            sobelFilter.bulgeDistort(widthCenter, heightCenter)
            sourcePicture.addTarget(sobelFilter)
            sobelFilter.useNextFrameForImageCapture()
            sourcePicture.processImage()
            let outputImage:UIImage = sobelFilter.imageFromCurrentFramebuffer()
            
            topImgHeight.constant = globalImgHeight
            topImgWidth.constant = globalImgHeight * globalRatio
            
            originalImg = outputImage
            postImg = outputImage
            
            btn1.hidden = false
            btn2.hidden = false
            btn3.hidden = false
            btn4.hidden = false
            btn5.hidden = false
            btn6.hidden = false
            
            bottomBack.hidden = false
            
            let ciImage1:CIImage = CIImage(image:outputImage)!
            let ciFilter1:CIFilter = CIFilter(name: "CIPhotoEffectInstant")!
            ciFilter1.setValue(ciImage1, forKey: "inputImage")
            let ciContext1:CIContext = CIContext(options: nil)
            let cgimg1:CGImageRef = ciContext1.createCGImage(ciFilter1.outputImage!, fromRect:ciFilter1.outputImage!.extent)
            let uiimage1:UIImage? = UIImage(CGImage: cgimg1, scale: 1.0, orientation:UIImageOrientation.Up)
            img1.hidden = false
            img1.backgroundColor = UIColor.clearColor()
            img1.setImage(uiimage1, forState: .Normal)
            img1.imageView!.contentMode = .ScaleAspectFit
            
            let ciImage2:CIImage = CIImage(image:outputImage)!
            let ciFilter2:CIFilter = CIFilter(name: "CIPhotoEffectChrome")!
            ciFilter2.setValue(ciImage2, forKey: "inputImage")
            let ciContext2:CIContext = CIContext(options: nil)
            let cgimg2:CGImageRef = ciContext2.createCGImage(ciFilter2.outputImage!, fromRect:ciFilter2.outputImage!.extent)
            let uiimage2:UIImage? = UIImage(CGImage: cgimg2, scale: 1.0, orientation:UIImageOrientation.Up)
            img2.hidden = false
            img2.backgroundColor = UIColor.clearColor()
            img2.setImage(uiimage2, forState: .Normal)
            img2.imageView!.contentMode = .ScaleAspectFit
            
            let ciImage3:CIImage = CIImage(image:outputImage)!
            let ciFilter3:CIFilter = CIFilter(name: "CIColorCrossPolynomial")!
            ciFilter3.setValue(ciImage3, forKey: "inputImage")
            let r: [CGFloat] = [1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            let g: [CGFloat] = [0.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            let b: [CGFloat] = [0.0, 0.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            ciFilter3.setValue(CIVector(values: r, count: 10), forKey: "inputRedCoefficients")
            ciFilter3.setValue(CIVector(values: g, count: 10), forKey: "inputGreenCoefficients")
            ciFilter3.setValue(CIVector(values: b, count: 10), forKey: "inputBlueCoefficients")
            let ciContext3:CIContext = CIContext(options: nil)
            let cgimg3:CGImageRef = ciContext3.createCGImage(ciFilter3.outputImage!, fromRect:ciFilter3.outputImage!.extent)
            let uiimage3:UIImage? = UIImage(CGImage: cgimg3, scale: 1.0, orientation:UIImageOrientation.Up)
            img3.hidden = false
            img3.backgroundColor = UIColor.clearColor()
            img3.setImage(uiimage3, forState: .Normal)
            img3.imageView!.contentMode = .ScaleAspectFit
            
            img4.hidden = false
            img4.backgroundColor = UIColor.clearColor()
            img4.setImage(outputImage, forState: .Normal)
            img4.imageView!.contentMode = .ScaleAspectFit
            
            let ciImage5:CIImage = CIImage(image:outputImage)!
            let ciFilter5:CIFilter = CIFilter(name: "CIPhotoEffectProcess")!
            ciFilter5.setValue(ciImage5, forKey: "inputImage")
            let ciContext5:CIContext = CIContext(options: nil)
            let cgimg5:CGImageRef = ciContext5.createCGImage(ciFilter5.outputImage!, fromRect:ciFilter5.outputImage!.extent)
            let uiimage5:UIImage? = UIImage(CGImage: cgimg5, scale: 1.0, orientation:UIImageOrientation.Up)
//            self.cameraImgView.image = originalImg
            img5.hidden = false
            img5.backgroundColor = UIColor.clearColor()
            img5.setImage(uiimage5, forState: .Normal)
            img5.imageView!.contentMode = .ScaleAspectFit
            
            let ciImage6:CIImage = CIImage(image:outputImage)!
            let ciFilter6:CIFilter = CIFilter(name: "CIPhotoEffectTransfer")!
            ciFilter6.setValue(ciImage6, forKey: "inputImage")
            let ciContext6:CIContext = CIContext(options: nil)
            let cgimg6:CGImageRef = ciContext6.createCGImage(ciFilter6.outputImage!, fromRect:ciFilter6.outputImage!.extent)
            let uiimage6:UIImage? = UIImage(CGImage: cgimg6, scale: 1.0, orientation:UIImageOrientation.Up)
            img6.hidden = false
            img6.backgroundColor = UIColor.clearColor()
            img6.setImage(uiimage6, forState: .Normal)
            img6.imageView!.contentMode = .ScaleAspectFit
            
            self.cameraImgView.image = originalImg
        }
        cameraImgView.hidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if upCameraBool == true {
            let picker:UIImagePickerController = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(picker, animated: true, completion: nil)
        }
        upCameraBool = false
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        let stampViews = cameraImgView.subviews
        for stampView in stampViews {
            stampView.removeFromSuperview()
        }
        
        let size = CGSize(width: 400, height: 400 * image.size.height / image.size.width)
        UIGraphicsBeginImageContext(size)
        image.drawInRect(CGRectMake(0, 0, size.width, size.height))
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        globalImg = resizeImage
        originalImg = resizeImage
        cameraImgView.hidden = false
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func twitter(sender: AnyObject) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Twitter?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            let title: String = "#BotoxCamera"
            controller.setInitialText(title)
            controller.addImage(self.postImg)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func facebook(sender: AnyObject) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Facebok?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            let title: String = "#BotoxCamera"
            controller.setInitialText(title)
            controller.addImage(self.postImg)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    var documentController:UIDocumentInteractionController?
    @IBAction func instagram(sender: AnyObject) {
        let imageData = UIImageJPEGRepresentation(postImg!, 1.0)
        let temporaryDirectory = NSTemporaryDirectory() as String
        let temporaryImagePath = temporaryDirectory.stringByAppendingPathComponent("postImg.igo")
        let boolValue = imageData!.writeToFile(temporaryImagePath, atomically: true)
        let fileURL = NSURL(fileURLWithPath: temporaryImagePath)
        documentController = UIDocumentInteractionController(URL: fileURL)
        documentController!.UTI = "com.instagram.exclusivegram"
        documentController!.presentOpenInMenuFromRect(
            self.view.frame,
            inView: self.view,
            animated: true
        )
    }
    
    @IBAction func reserve(sender: UIButton) {
        if postImg != nil {
            UIImageWriteToSavedPhotosAlbum(postImg!, self, nil, nil)
            let alertController = UIAlertController(title: "Complete", message: "Successfully Saved.", preferredStyle: .Alert)
            let otherAction = UIAlertAction(title: "OK", style: .Default) {
                action in self.dismissViewControllerAnimated(true, completion: nil)
            }
            alertController.addAction(otherAction)
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func takeAgain(sender: UIButton) {
        let picker:UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func selectImg(sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePickerController.allowsEditing = false
        imagePickerController.delegate = self
        self.presentViewController(imagePickerController,animated:true ,completion:nil)
    }
    
    @IBAction func btn1(sender: UIButton) {
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectInstant")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        self.cameraImgView.image = image2
        
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn2(sender: UIButton) {
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectChrome")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        self.cameraImgView.image = image2
        
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn3(sender: UIButton) {
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIColorCrossPolynomial")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let r: [CGFloat] = [1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        let g: [CGFloat] = [0.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        let b: [CGFloat] = [0.0, 0.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        ciFilter.setValue(CIVector(values: r, count: 10), forKey: "inputRedCoefficients")
        ciFilter.setValue(CIVector(values: g, count: 10), forKey: "inputGreenCoefficients")
        ciFilter.setValue(CIVector(values: b, count: 10), forKey: "inputBlueCoefficients")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        self.cameraImgView.image = image2
        
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn4(sender: UIButton) {
        //        let ciImage:CIImage = CIImage(image:originalImg!)!
        //        let ciFilter:CIFilter = CIFilter(name: "CIToneCurve" )!
        //        ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
        //        ciFilter.setValue(CIVector(x: 0.0, y: 0.0), forKey: "inputPoint0")
        //        ciFilter.setValue(CIVector(x: 0.25, y: 0.1), forKey: "inputPoint1")
        //        ciFilter.setValue(CIVector(x: 0.5, y: 0.5), forKey: "inputPoint2")
        //        ciFilter.setValue(CIVector(x: 0.75, y: 0.9), forKey: "inputPoint3")
        //        ciFilter.setValue(CIVector(x: 1.0, y: 1.0), forKey: "inputPoint4")
        //        let ciContext:CIContext = CIContext(options: nil)
        //        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        //        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        
        self.cameraImgView.image = originalImg
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn5(sender: UIButton) {
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectProcess")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        self.cameraImgView.image = image2
        
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn6(sender: UIButton) {
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectTransfer")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        self.cameraImgView.image = image2
        
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID(nendID,
                          spotID: spotID)
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID(nendID,
                          spotID: spotID)
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}

